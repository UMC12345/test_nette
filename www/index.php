<!DOCTYPE html>
<html>
<head>
<title>Creating using Nette Project</title>
</head>
<body>
<h1>My First Nette Project </h1>
<p>After editing the Nette project uploaded to git!</p>
</body>
</html>
<?php
$container = require __DIR__ . '/../app/bootstrap.php';

$container->getByType(Nette\Application\Application::class)
	->run();
